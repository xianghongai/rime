function calc_translator(input, seg)
  if string.find(input, 'calc') ~= nil then -- 匹配 calc 开头的字符串
      local _, _, a, operation, b = string.find(input, "calc(%d+%.?%d*)([%+%-%*/])(%d+%.?%d*)")
      local result = 0
      if operation == '+' then
          result = a + b
      elseif operation == '-' then
          result = a - b
      elseif operation == '*' then
          result = a * b
      elseif operation == '/' then
          result = a / b
      end
      yield(Candidate("calc", seg.start, seg._end, result, "计算器"))
  end
end

return calc_translator
