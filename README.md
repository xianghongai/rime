# 也致第一次安装 Rime 的你

我的需求是需要一套跨平台的输入方案:

因此，我选择了 [Rime](http://rime.im/)。

## 0、安装

先按系统平台安装 Rime，本笔记以 **Windows 平台/五笔输入方案**为例：

下载安装[小狼毫 Weasel](http://rime.im/download/)和[编辑器 visual studio code](https://code.visualstudio.com/)。<small>保证文件UTF-8编码</small>

安装 Rime 之后，有默认的预置输入方案，各输入方案切换初始快捷键是 <kbd>Ctrl</kbd> + <kbd>~</kbd>。

如果没有适合自己的输入方案，看完本笔记第2、3、4章节后，在浏览器中打开 [Plum](https://github.com/rime/plum)项目，<kbd>Ctrl</kbd> + <kbd>F</kbd> 查找并下载相关输入方案到**用户配置目录**，**重新部署**。

如：查找 `wubi`，打开[rime-wubi](https://github.com/rime/rime-wubi)项目，点绿色按钮 `Download ZIP` 到桌面解压，把几个 `*.yaml` 后缀文件剪切到 `%appdata%\Rime` <small>(复制，<kbd>Windows</kbd> + <kbd>R</kbd> 运行中粘贴回车即可进入)</small> 目录，重新部署。

Plum 是 Rime 配置管理器和输入模式库。

## 1、认识 “Rime”

要配置和操控 Rime，首先要知道它是什么，

Rime：

- 一套**文字输入解决方案**，由基础引擎<sup>\*</sup>、跨操作平台发行<sup>\*</sup>、多种输入方案 (schema + dict)<sup>\*</sup>组成，

- 支持“拼音、双拼、注音、声调、五笔、仓颉”等**音码**和**形码**输入；

- 支持“吴语、粤语，中古汉语”等多种方言拼音；

- 支持 Windows、Linux、Android、Mac OS X 等作业平台。
    + [ibus-rime](https://github.com/rime/home/wiki/RimeWithIBus) → Linux
    + [Weasel/小狼毫](https://bintray.com/rime/weasel/release) → Windows
    + [Squirrel/鼠须管](https://bintray.com/lotem/rime/Squirrel) → Mac OS X
    + [同文輸入法](https://github.com/osfans/trime) → Android

- 繁体拼音能正确的输入繁体字。

上面有标<sup>\*</sup>号几个部分，就是我们要配置调教的部分。

## 2、认识“部署”与“同步”

每当更改了配置文件，就要进行一次**部署**以生效预期功能，同时执行一次**同步**以备份相关配置。

Windows 部署：

- 在开始菜单中找到 `小狼毫輸入法 → 【小狼毫】重新部署`，执行即生效；

- 在 `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\小狼毫輸入法` 中执行 `【小狼毫】重新部署`；

- 如果配置中有开启托盘图标，右键选择`重新部署`。

其它保作系统更简单，如iBus在输入法切换下拉菜单即可`部署`，鼠鬚管在系统语言文字选单中选择`重新部署`

同步亦然，这里同步只是根据用户配置目录 `installation.yaml` 中的 `installation_id` 和 `sync_dir` 生成了配置和词典文件，至于同步之后备份在何处，是需要用户自己安排的，如各 SaaS 厂商的云盘。

## 3、认识“源文件目录”和“用户配置目录”

“源文件目录”和“用户配置目录”是整个输入方案的配置文件所在目录。

输入方案**源文件目录**，升级或重装会被替换，用户不能修改：

```
【中州韻】 /usr/share/rime-data/
【小狼毫】 "安装目录\data"
【鼠鬚管】 "/Library/Input Methods/Squirrel.app/Contents/SharedSupport/"
```

输入方案**用户配置目录**，用户可以部分修改：

```
【中州韻】 ~/.config/ibus/rime/
          ~/.config/fcitx/rime/
【小狼毫】 "%appdata%\Rime"
【鼠鬚管】 ~/Library/Rime/
```

## 4、认识“用户配置目录”数据文件分布

数据文件和目录在各版本中可能会有不同。

- 主要文件
  + `default.yaml`，全局设定
  + `weasel.yaml`，发行版设定
  + `installation.yaml`，安装信息(可配置同步目录)
  + `user.yaml`，用户状态信息
  + `*.schema.yaml`，输入方案
  + `*.dict.yaml`，输入方案配套的词典源文件，主码表
  + `*.custom.yaml`<sup>+</sup>，用户对Rime全局配置、各输入方案配置、自制输入方案等定制文件

- 用户配置同步目录及词典文件
  + `*.userdb`，Rime 记录用户输入习惯的目录文件
  + `UUID/`，用户配置同步目录
  + `UUID/*.userdb.txt`，Rime生成的用以同步的用户词典

- Rime 编译生成的二进制文件：
  + `build/*`
  + `*.prism.bin`，棱镜文件
  + `*.reverse.bin`，反查词典
  + `*.table.bin`，固态词典

```bash
├── README.md                               # 当前说明文档
├── rime.lua                                # 配置文件 - 可以输出系统变量的函数
├── default.custom.yaml                     # 配置文件 - 自定义一些输入法的功能：标点，二三候选等
├── squirrel.custom.yaml                    # 配置文件 - 鼠须管 (for macOS) 输入法候选词界面
├── weasel.custom.yaml                      # 配置文件 - 小狼毫 (for Windows) 输入法候选词界面
├── numbers.schema.yaml                     # 输入方案 - 大写数字
├── pinyin_simp.dict.yaml                   # 词库文件 - 简体拼音码表 - 五笔中拼音输入需要的
├── pinyin_simp.schema.yaml                 # 输入方案 - 简体拼音
├── wubi86_pinyin.schema.yaml               # 输入方案 - 五笔拼音混输
├── wubi86.schema.yaml                      # 输入方案 - 五笔
├── wubi86.dict.yaml                        # 词库文件 - 五笔主码表
├── wubi86_trad.schema.yaml                 # 输入方案 - 五笔简入繁出
├── wubi86_user.dict.yaml                   # 词库文件 - 用户私人词库
└── wubi86_extra.dict.yaml                  # 词库文件 - 扩展词库
```

## 5、折腾

要折腾的不是很多，普通用户安装即用，我折腾它主要是按键部分和我开发工具冲突，还有增强输入方案。

我们要配置四个文件，因为以后要升级，不能直接在原作者默认文件内修改和调整，所以要这样建立文件关联：

- **基础引擎框架配置 `default.yaml`** → `default.custom.yaml`，输入法按键、方案预选等全局设定
- **输入方案配置 `wubi86.schema.yaml`** → `wubi86.custom.yaml`，每种形码、音码输入方案都可以单独配置
- **增强输入方案配置 `symbols.yaml`** → `symbols.custom.yaml`，如： /jq 将调用二十四节气名
- **Windows发行版配置 `weasel.yaml`** → `weasel.custom.yaml`，如进入相关应用程序，转换为英文状态

我进行的配置有：

- `Control+Shift+F8` 调出输入方案设定，原来的 <code>Ctrl+`</code> 会与大部分软件的关键按键冲突
- `,`、`.`输入时左右翻页
- `Enter`清屏，`;`、`'`，输入时第二、三候选词选定
- 因为经常编码和用Markdown语法，很多字符为英文状态下字符，如`[]()`等
- `/`用来触发特殊字符输入，如`/fh`、`/0~9`、`/jt`等，具体参考`symbols.custom.yaml`文件
- `Shift+Delete` 或 `Control+Delete`（Mac `Shift+Fn+Delete`）删除候选词

### 5.1 目的

- 覆盖原默认配置
- 选择和扩展功能

### 5.2 调教

- 先三分钟了解一下 [yaml](http://www.ruanyifeng.com/blog/2016/07/yaml.html) 配置文件语言
- 打开配置目录：Windows + R : `$ %appdata%/rime`
- 用 VS Code 编辑器新建四个文件：`default.custom.yaml`，`wubi86.custom.yaml`，`symbols.custom.yaml`，`weasel.custom.yaml`，这四个文件配置完可以备份一份，方便在其它计算机上用。

#### 5.2.1 基础引擎框架配置 ：default.custom.yaml

```yaml
# 此文档是对 rime 基础引擎框架配置

customization:
  distribution_code_name: Weasel
  distribution_version: 0.9.30
  generator: "Rime::SwitcherSettings"
  modified_time: "Mon Mar 12 14:30:01 2018"
  rime_version: 1.3.0

patch:
  "translator/enable_user_dict": false # 关闭用户词典和字频调整
  "translator/enable_sentence": false # 关闭码表输入法连打
  "key_binder/bindings":
    - {accept: "Control+Shift+space", toggle: full_shape, when: always} # 全半角切换
    - {accept: "Control+period", toggle: ascii_punct, when: always}
    - {accept: comma, send: Page_Up, when: paging}
    - {accept: period, send: Page_Down, when: has_menu}
  "ascii_composer/switch_key":
    Caps_Lock: commit_code
    Control_L: inline_ascii
    Control_R: noop
    Shift_L: commit_code # 左右Shift上屏编码并切换为英文状态，inline_ascii 设定在有输入时不能切换至英文状态
    Shift_R: commit_code #

  # 原来的 Ctrl + ` 会与大部分软件的关键按键冲突
  "switcher/hotkeys":
    - "Control+Shift+F8"

  # 除了输入翻页，应该取消所有的默认快捷鍵，输入法就是纯粹的输入用，用户按需分配快捷键，设定默认快捷键会与作业系统及其它软件产生冲突，适得其反，这样与用搜狐、腾讯输入法无二。
  # minus/减号，equal/等号，comma/逗号，period/句号，exclam/感叹号，numbersign/井号，percent/百分号，semicolon/分号，apostrophe/单引号
  key_binder/bindings:
    # hotkey switch
    - { when: always, accept: Shift+space, toggle: full_shape }
    - { when: always, accept: Control+period, toggle: ascii_punct }
    - { accept: comma, send: Page_Up, when: paging }
    - { accept: period, send: Page_Down, when: has_menu }


  # 选定五笔等输入方案
  schema_list:
    - {schema: wubi86}
    - {schema: wubi_pinyin}
    - {schema: wubi_trad}
    - {schema: luna_pinyin_simp}
    - {schema: luna_pinyin_tw}
    - {schema: terra_pinyin}
```

#### 5.2.2 输入法配置 ：wubi86.custom.yaml

这里是五笔配置，回车用以清屏了，不上屏字符和字母。

如果是在拼音中，如采用语句流模式(fluid_editor)，回车键、标点符号用以上屏字符，空格用以断词；
如采用普通模式(express_editor)，回车用以上屏字母；

因此，五笔中不适合中英文混合输入，拼音中则适合。

```yaml
xianghongai@gmail.com
# 此文档是对输入法的配置

patch:
  # 标点及特殊表情，引入 symbols.custom.yaml 文件，设定触发条件
  'punctuator/import_preset': symbols.custom
  'recognizer/patterns/punct': "^/([a-z]+|[0-9])$"

  # 1.回车清屏(Escape)/上屏一候选词，2.分号上屏二候选词，3.引号上屏三候选词
  "key_binder/bindings":
    - { when: composing, accept: Return, send: Escape }
    - { when: has_menu, accept: Return, send: Escape }
    # - {when: composing, accept: space, send: Escape}
    # - {when: has_menu, accept: space, send: space}
    - { when: has_menu, accept: semicolon, send: 2 }
    - { when: has_menu, accept: apostrophe, send: 3 }
  # 更改‘西文’为‘英文’，‘增广’为‘扩展集’
  switches:
  - name: ascii_mode
    reset: 0
    states: ["中文", "英文"]
  - name: full_shape
    states: ["半角", "全角"]
  - name: ascii_punct
    states: ["，。", "，．"]
  - name: extended_charset
    states: ["通用", "扩展集"]
```

#### 5.2.3 Windows 操作平台配置：weasel.custom.yaml

```yaml
xianghongai@gmail.com
# 此文档是对rime在Windows平台上的配置

customization:
  distribution_code_name: Weasel
  distribution_version: 0.9.30
  generator: "Weasel::UIStyleSettings"
  modified_time: "Fri Mar 09 15:08:29 2018"
  rime_version: 1.3.0

# 进入相关应用程序，转换为英文状态
patch:
  "app_options/devenv.exe":
    ascii_mode: true
  "app_options/illustrator.exe":
    ascii_mode: true
  "app_options/photoshop.exe":
    ascii_mode: true
  "app_options/plantsvszombies":
    ascii_mode: true
  "app_options/winkawaks.exe":
    ascii_mode: true
  "app_options/sublime_text.exe":
    ascii_mode: true
  "app_options/Code.exe":
    ascii_mode: true

  # 设定主题
  "style/color_scheme": ink

  # 设定显示托盘图标，在调配置时，方便右键图标快速执行“重新部署”，以及其它便捷入口。调配完日常使用就 false 隐藏掉吧。
  # 没有托盘图标，Windows+R: $ "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\小狼毫輸入法"，里面有相关操作项
  # Windows 10下已经发现问题：切换到小狼毫輸入法，Windows徽标就失灵，必须切换至其它输入法，Ctrl+Shift+Esc 中 “Windows 资源管理器” 鼠标右键“重新启动(R)”，才能正常使用。
  "style/display_tray_icon": false
```

#### 5.2.4 增强输入方案配置：symbols.custom.yaml

直接从原作者复制内容过来，进行以下修改：

```
# / 用来触发增强输入，如： /jq 将调用二十四节气名
# 星号直接上屏 *
# 圆括号为英文状态下的() ，建议文字工作者的你换成中文状态下的（）
# 顿号需要按键 \
# 井号直接上屏 #
# 添加常用网址
```

代码过多就不贴了；

配置完重新部署，并执行同步以备份相关配置文件。

## 6、存在问题与选择

操作存在极小可能的不稳定性，以及一些兼容性问题，在权衡了隐私性、调教性后，我能接受。

开始菜单中，将“小狼毫算法服務”、“【小狼毫】重新部署”和“【小狼毫】用戶資料同步” Pin to Start，方便应对不稳定性。

---

扩展阅读与资源：

- [Rime 新手须知的使用方法和技巧](https://github.com/rime/home/wiki/UserGuide)
- [Rime 定制指南(初阶)](https://github.com/rime/home/wiki/CustomizationGuide)
- [Rime 输入方案(中阶)](https://github.com/rime/home/wiki/RimeWithSchemata)
- [Rime 程序开发(高阶)](http://rime.im/code/)
- [半月湾C：致第一次安装RIME的你](http://tieba.baidu.com/p/3288634121)
- [wikipedia: YAML](https://zh.wikipedia.org/wiki/YAML)

---


许可协议：自由转载-保持署名-非商业性使用-禁止演绎 ([CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.zh))
By [小可](https://xinlu.ink) from [https://xinlu.ink/tech/rime.html](https://xinlu.ink/tech/rime.html)